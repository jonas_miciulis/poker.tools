package eu.eagercode.pokertools.api.controllers;

import eu.eagercode.pokertools.api.dao.BlindsRepository;
import eu.eagercode.pokertools.api.entities.Blinds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/blinds")
public class BlindsController {

    @Autowired
    private BlindsRepository blindsRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Blinds findById(@PathVariable(value = "id") Long id) {
        return blindsRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Blinds> findAll() {
        return blindsRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public Blinds put(@RequestBody Blinds blinds) {
        return blindsRepository.save(blinds);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Blinds post(@RequestBody Blinds blinds) {
        return blindsRepository.save(blinds);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        Blinds blinds = blindsRepository.findOne(id);
        blindsRepository.delete(blinds);
    }

}
