package eu.eagercode.pokertools.api.dao;

import eu.eagercode.pokertools.api.entities.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends org.springframework.data.repository.Repository<User, Long> {

    User findOne(Long id);

    User findByUsername(String username);

    User save(User entity);

}
