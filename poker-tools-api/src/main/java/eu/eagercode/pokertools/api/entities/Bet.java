package eu.eagercode.pokertools.api.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "bet")
public class Bet {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @SequenceGenerator(name = "betIdGenerator", sequenceName = "bet_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "betIdGenerator")
    private Long id;

    @Column(name = "level", nullable = false)
    @NotNull
    @Min(0)
    private Integer level;

    @Column(name = "big_blind", nullable = false)
    @NotNull
    @Min(0)
    private BigDecimal bigBlind;

    @Column(name = "small_blind", nullable = false)
    @NotNull
    @Min(0)
    private BigDecimal smallBlind;

    @Column(name = "ante")
    @Min(0)
    private BigDecimal ante;

}
