package eu.eagercode.pokertools.api.controllers;

import eu.eagercode.pokertools.api.dao.UserRepository;
import eu.eagercode.pokertools.api.entities.Blinds;
import eu.eagercode.pokertools.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public User findById(@PathVariable(value = "id") Long id) {
        return userRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public User put(@RequestBody User user) {
        return userRepository.save(user);
    }

}
