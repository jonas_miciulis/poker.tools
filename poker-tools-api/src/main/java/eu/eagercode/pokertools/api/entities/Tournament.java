package eu.eagercode.pokertools.api.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Table(name = "tournament")
public class Tournament {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @SequenceGenerator(name = "tournamentIdGenerator", sequenceName = "tournament_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tournamentIdGenerator")
    private Long id;

    @Column(name = "caption", nullable = false, length = 500)
    @NotNull
    private String caption;

    @Column(name = "level_time", nullable = false)
    @NotNull
    private Integer levelTime;

    @Column(name = "chips_at_beginning")
    private Integer chipsAtBeginning;

    @JoinColumn(name = "blinds_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    private Blinds blinds;

}
