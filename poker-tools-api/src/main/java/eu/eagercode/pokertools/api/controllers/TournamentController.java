package eu.eagercode.pokertools.api.controllers;

import eu.eagercode.pokertools.api.dao.TournamentRepository;
import eu.eagercode.pokertools.api.entities.Tournament;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/tournament")
public class TournamentController {

    @Autowired
    private TournamentRepository tournamentRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Tournament findById(@PathVariable(value = "id") Long id) {
        return tournamentRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Tournament> findAll() {
        return tournamentRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public Tournament put(@RequestBody Tournament tournament) {
        return tournamentRepository.save(tournament);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Tournament post(@RequestBody Tournament tournament) {
        Tournament oldTournament = tournamentRepository.save(tournament);
        return tournamentRepository.findOne(oldTournament.getId());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        Tournament tournament = tournamentRepository.findOne(id);
        tournamentRepository.delete(tournament);
    }

}
