package eu.eagercode.pokertools.api.dao;

import eu.eagercode.pokertools.api.entities.Blinds;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlindsRepository extends org.springframework.data.repository.Repository<Blinds, Long> {

    List<Blinds> findAll();

    Blinds findOne(Long id);

    Blinds save(Blinds entity);

    void delete(Blinds blinds);

}
