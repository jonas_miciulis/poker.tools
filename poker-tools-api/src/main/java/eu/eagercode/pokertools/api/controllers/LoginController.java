package eu.eagercode.pokertools.api.controllers;

import eu.eagercode.pokertools.api.dao.UserRepository;
import eu.eagercode.pokertools.api.entities.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import java.util.Date;


@RestController
@RequestMapping("/auth")
public class LoginController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@RequestBody User login) throws ServletException {
        User user = userRepository.findByUsername(login.getUsername());

        if (user == null) {
            throw new ServletException("User email not found.");
        }

        if (!login.getPassword().equals(user.getPassword())) {
            throw new ServletException("Invalid login. Please check your name and password.");
        }

        return Jwts.builder().setSubject(login.getUsername()).claim("roles", "user")
                .setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, "secretkey").compact();
    }

}