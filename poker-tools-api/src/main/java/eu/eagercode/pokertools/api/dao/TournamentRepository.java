package eu.eagercode.pokertools.api.dao;

import eu.eagercode.pokertools.api.entities.Tournament;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TournamentRepository extends org.springframework.data.repository.Repository<Tournament, Long> {

    List<Tournament> findAll();

    Tournament findOne(Long id);

    Tournament save(Tournament entity);

    void delete(Tournament tournament);

}
