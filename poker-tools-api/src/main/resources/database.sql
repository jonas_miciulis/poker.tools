--CREATE USER pokertools WITH PASSWORD 'kumpis_240' SUPERUSER;
DROP SCHEMA IF EXISTS poker_tools CASCADE;
CREATE SCHEMA poker_tools;

CREATE TABLE poker_tools.user (
  id SERIAL NOT NULL,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(64) NOT NULL,
  first_name VARCHAR(50),
  last_name VARCHAR(50),
  email VARCHAR(500) NOT NULL
);

ALTER TABLE poker_tools.user ADD CONSTRAINT user_pkey PRIMARY KEY (id);
ALTER TABLE poker_tools.user ADD CONSTRAINT user_username_ukey UNIQUE (username);

INSERT INTO poker_tools.user (username, password, first_name, last_name, email) VALUES ('admin', 'admin', 'Jonas', 'Miciulis', 'eager.code@gmail.com');

CREATE TABLE poker_tools.blinds (
    id SERIAL NOT NULL,
    caption VARCHAR(500) NOT NULL
);

ALTER TABLE poker_tools.blinds ADD CONSTRAINT blinds_pkey PRIMARY KEY (id);

CREATE TABLE poker_tools.bet (
    id SERIAL NOT NULL,
    blinds_id INTEGER NOT NULL REFERENCES poker_tools.blinds(id),
    level INTEGER NOT NULL,
    big_blind NUMERIC(8, 2) NOT NULL,
    small_blind NUMERIC(8, 2) NOT NULL,
    ante NUMERIC(8, 2)
);

ALTER TABLE poker_tools.bet ADD CONSTRAINT bet_pkey PRIMARY KEY (id);

CREATE TABLE poker_tools.tournament (
  id SERIAL NOT NULL,
  caption VARCHAR(500) NOT NULL,
  level_time INTEGER NOT NULL,
  chips_at_beginning INTEGER,
  blinds_id INTEGER NOT NULL REFERENCES poker_tools.blinds(id)
);

ALTER TABLE poker_tools.tournament ADD CONSTRAINT tournament_pkey PRIMARY KEY (id);

