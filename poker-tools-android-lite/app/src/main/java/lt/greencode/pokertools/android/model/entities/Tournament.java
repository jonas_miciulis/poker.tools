package lt.greencode.pokertools.android.model.entities;

import java.io.Serializable;

public class Tournament implements Serializable {

    private Long id;
    private String caption;
    private Integer levelTime;
    private Integer chipsAtBeginning;
    private Blinds blinds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Integer getLevelTime() {
        return levelTime;
    }

    public void setLevelTime(Integer levelTime) {
        this.levelTime = levelTime;
    }

    public Integer getChipsAtBeginning() {
        return chipsAtBeginning;
    }

    public void setChipsAtBeginning(Integer chipsAtBeginning) {
        this.chipsAtBeginning = chipsAtBeginning;
    }

    public Blinds getBlinds() {
        return blinds;
    }

    public void setBlinds(Blinds blinds) {
        this.blinds = blinds;
    }

    @Override
    public String toString() {
        return caption;
    }

}
