package lt.greencode.pokertools.android.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import lt.greencode.pokertools.R;
import lt.greencode.pokertools.android.model.common.TaskResult;
import lt.greencode.pokertools.android.model.entities.Bet;
import lt.greencode.pokertools.android.model.entities.Blinds;
import lt.greencode.pokertools.android.tasks.HttpCallTask;
import lt.greencode.pokertools.android.utilities.MarshallingUtils;

public class EditBlindsActivity extends BaseActivity {

    public static final String EXTRA_BLINDS_ID = "BLINDS_ID";

    private EditText editTextCaption;
    private EditText editTextNumberOfLevels;
    private TableLayout tableLayoutBlinds;
    private Button buttonSave;
    private Long blindsId;
    private Map<Integer, Bet> oldBets = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blinds_edit);

        editTextCaption = (EditText) findViewById(R.id.blindsEditEditTextCaption);
        editTextNumberOfLevels = (EditText) findViewById(R.id.blindsEditEditTextNumberOfLevels);
        tableLayoutBlinds = (TableLayout) findViewById(R.id.blindsEditTableLayoutBlinds);
        buttonSave = (Button) findViewById(R.id.blindsEditButtonSave);

        editTextNumberOfLevels.addTextChangedListener(new EditTextNumberOfLevelsListener());
        buttonSave.setOnClickListener(new ButtonSaveListener());

        if (getIntent().hasExtra(EditBlindsActivity.EXTRA_BLINDS_ID)) {
            blindsId = getIntent().getExtras().getLong(EditBlindsActivity.EXTRA_BLINDS_ID);
            new GetBlindsTask().execute();
        }
    }

    class EditTextNumberOfLevelsListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String numberOfLevelsText = editTextNumberOfLevels.getText().toString();
            int numberOfLevels = !numberOfLevelsText.isEmpty() ? Integer.valueOf(numberOfLevelsText) : 0;

            if (numberOfLevels > tableLayoutBlinds.getChildCount() - 1) {
                for (int i = tableLayoutBlinds.getChildCount() - 1; i < numberOfLevels; i++) {
                    LayoutInflater inflater = (LayoutInflater) EditBlindsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    TableRow row = (TableRow) inflater.inflate(R.layout.blinds_edit_row, null);

                    TextView textViewLevel = (TextView) row.findViewById(R.id.blindsEditRowTextViewLevel);
                    EditText editTextSmallBlind = (EditText) row.findViewById(R.id.blindsEditRowEditTextSmallBlind);
                    EditText editTextBigBlind = (EditText) row.findViewById(R.id.blindsEditRowEditTextBigBlind);
                    EditText editTextAnte = (EditText) row.findViewById(R.id.blindsEditRowEditTextAnte);

                    Bet oldBet = oldBets.get(i);

                    textViewLevel.setText(String.valueOf(i + 1));
                    if (oldBet != null) {
                        editTextSmallBlind.setText(oldBet.getSmallBlind() != null ? oldBet.getSmallBlind().stripTrailingZeros().toPlainString() : "");
                        editTextBigBlind.setText(oldBet.getBigBlind() != null ? oldBet.getBigBlind().stripTrailingZeros().toPlainString() : "");
                        editTextAnte.setText(oldBet.getAnte() != null ? oldBet.getAnte().stripTrailingZeros().toPlainString() : "");
                    }

                    tableLayoutBlinds.addView(row);
                }
            } else {
                for (int i = tableLayoutBlinds.getChildCount() - 1; i > numberOfLevels; i--) {
                    TableRow tableRow = (TableRow) tableLayoutBlinds.getChildAt(i);
                    String level = ((TextView) tableRow.findViewById(R.id.blindsEditRowTextViewLevel)).getText().toString();
                    String bigBlind = ((EditText) tableRow.findViewById(R.id.blindsEditRowEditTextBigBlind)).getText().toString();
                    String smallBlind = ((EditText) tableRow.findViewById(R.id.blindsEditRowEditTextSmallBlind)).getText().toString();
                    String ante = ((EditText) tableRow.findViewById(R.id.blindsEditRowEditTextAnte)).getText().toString();

                    Bet bet = new Bet();
                    bet.setLevel(Integer.valueOf(level));
                    bet.setBigBlind(!bigBlind.isEmpty() ? new BigDecimal(bigBlind) : null);
                    bet.setSmallBlind(!smallBlind.isEmpty() ? new BigDecimal(smallBlind) : null);
                    bet.setAnte(!ante.isEmpty() ? new BigDecimal(ante) : null);

                    oldBets.put(i - 1, bet);

                    tableLayoutBlinds.removeViewAt(i);
                }
            }
        }

    }

    class ButtonSaveListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Blinds blinds = new Blinds();

            String caption = editTextCaption.getText().toString();

            blinds.setId(blindsId);
            blinds.setCaption(caption);

            for (int i = 1; i < tableLayoutBlinds.getChildCount(); i++) {
                TableRow tableRow = (TableRow) tableLayoutBlinds.getChildAt(i);
                String level = ((TextView) tableRow.findViewById(R.id.blindsEditRowTextViewLevel)).getText().toString();
                String bigBlind = ((EditText) tableRow.findViewById(R.id.blindsEditRowEditTextBigBlind)).getText().toString();
                String smallBlind = ((EditText) tableRow.findViewById(R.id.blindsEditRowEditTextSmallBlind)).getText().toString();
                String ante = ((EditText) tableRow.findViewById(R.id.blindsEditRowEditTextAnte)).getText().toString();

                Bet bet = new Bet();
                bet.setLevel(Integer.valueOf(level));
                bet.setBigBlind(!bigBlind.isEmpty() ? new BigDecimal(bigBlind) : null);
                bet.setSmallBlind(!smallBlind.isEmpty() ? new BigDecimal(smallBlind) : null);
                bet.setAnte(!ante.isEmpty() ? new BigDecimal(ante) : null);

                blinds.getBets().add(bet);
            }

            String json = MarshallingUtils.convertObjectToJsonBytes(blinds);
            new SaveBlindsTask(json).execute();
        }

    }

    class GetBlindsTask extends HttpCallTask<Blinds> {

        public GetBlindsTask() {
            super(EditBlindsActivity.this, "/blinds/" + blindsId, GET, FORM_URLENCODED, JSON, null, Blinds.class, false, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<Blinds> result) {
            Blinds blinds = result.getResult();

            editTextCaption.setText(blinds.getCaption());
            for (int i = 0; i < blinds.getBets().size(); i++) {
                Bet bet = blinds.getBets().get(i);

                LayoutInflater inflater = (LayoutInflater) EditBlindsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                TableRow row = (TableRow) inflater.inflate(R.layout.blinds_edit_row, null);

                TextView textViewLevel = (TextView) row.findViewById(R.id.blindsEditRowTextViewLevel);
                EditText editTextSmallBlind = (EditText) row.findViewById(R.id.blindsEditRowEditTextSmallBlind);
                EditText editTextBigBlind = (EditText) row.findViewById(R.id.blindsEditRowEditTextBigBlind);
                EditText editTextAnte = (EditText) row.findViewById(R.id.blindsEditRowEditTextAnte);

                textViewLevel.setText(String.valueOf(bet.getLevel()));
                editTextSmallBlind.setText(bet.getSmallBlind().stripTrailingZeros().toPlainString());
                editTextBigBlind.setText(bet.getBigBlind().stripTrailingZeros().toPlainString());
                editTextAnte.setText(bet.getAnte() != null ? bet.getAnte().stripTrailingZeros().toPlainString() : "");

                tableLayoutBlinds.addView(row);
            }

            editTextNumberOfLevels.setText(String.valueOf(blinds.getBets().size()));
        }

    }

    class SaveBlindsTask extends HttpCallTask<Blinds> {


        public SaveBlindsTask(String data) {
            super(EditBlindsActivity.this, "/blinds", PUT, JSON, JSON, data, Blinds.class, false, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<Blinds> result) {
            if (result.getError() == null) {
                Toast.makeText(EditBlindsActivity.this, R.string.blinds_blindsSuccessfullySaved, Toast.LENGTH_LONG).show();

                Intent intent = new Intent();
                intent.putExtra(EditTournamentActivity.EXTRA_BLINDS, result.getResult());
                setResult(Activity.RESULT_OK, intent);

                finish();
            }
        }

    }

}
