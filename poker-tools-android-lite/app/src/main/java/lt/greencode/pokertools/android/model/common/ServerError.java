package lt.greencode.pokertools.android.model.common;

public class ServerError extends RuntimeException {

    private String error;

    public ServerError() {
    }

    public ServerError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return error;
    }

}
