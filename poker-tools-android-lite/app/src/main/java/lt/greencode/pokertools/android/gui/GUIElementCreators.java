package lt.greencode.pokertools.android.gui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.WindowManager;
import lt.greencode.pokertools.R;

public class GUIElementCreators {

    public static void showAlertDialog(Context context, String title, DialogInterface.OnClickListener positiveButtonListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.common_ok), positiveButtonListener);
        builder.setNegativeButton(context.getString(R.string.common_cancel), null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
