package lt.greencode.pokertools.android.activities;

import android.app.Activity;

public abstract class BaseActivity extends Activity {

    public static final String SERVER_URL = "http://10.0.2.2:8080/api";

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";

    public static final String JSON = "application/json";
    public static final String FORM_URLENCODED = "application/x-www-form-urlencoded";

}
