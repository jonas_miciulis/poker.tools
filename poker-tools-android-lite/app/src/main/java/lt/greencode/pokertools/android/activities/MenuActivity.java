package lt.greencode.pokertools.android.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import lt.greencode.pokertools.R;
import lt.greencode.pokertools.android.model.common.TaskResult;
import lt.greencode.pokertools.android.tasks.HttpCallTask;

public class MenuActivity extends BaseActivity {

    private ListView listViewMenu;
    private String sessionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        listViewMenu = (ListView) findViewById(R.id.menuListViewMenu);

        listViewMenu.setOnItemClickListener(new ListViewMenuListener());
        listViewMenu.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getMenuItems()));

        SharedPreferences sharedPreferences = getSharedPreferences(MenuActivity.this.getPackageName(), Context.MODE_PRIVATE);
        sessionId = sharedPreferences.getString(LoginActivity.EXTRA_JSESSION_ID, null);
    }

    private List<String> getMenuItems() {
        List<String> menuList = new ArrayList<>();
        menuList.add(getString(R.string.menu_myTournaments).toUpperCase(Locale.getDefault()));
        menuList.add(getString(R.string.menu_blinds).toUpperCase(Locale.getDefault()));
        menuList.add(getString(R.string.menu_logout).toUpperCase(Locale.getDefault()));

        return menuList;
    }

    class ListViewMenuListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String title = (String) parent.getItemAtPosition(position);

            if (title.equalsIgnoreCase(getString(R.string.menu_myTournaments))) {
                startActivity(new Intent(MenuActivity.this, TournamentsListActivity.class));
            } else if (title.equalsIgnoreCase(getString(R.string.menu_blinds))) {
                startActivity(new Intent(MenuActivity.this, BlindsListActivity.class));
            } else if (title.equalsIgnoreCase(getString(R.string.menu_logout))) {
                finish();
            }
        }

    }

}
