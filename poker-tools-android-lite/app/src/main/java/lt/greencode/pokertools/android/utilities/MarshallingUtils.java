package lt.greencode.pokertools.android.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class MarshallingUtils {

    public static <T> T convertJsonToObject(String json, Class<T> clazz) throws IOException {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json, clazz);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static <T> List<T> convertJsonToObjectsList(String json, Class<T> clazz) throws IOException {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json, objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static <T> String convertObjectToJsonBytes(T object) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return new String(objectMapper.writeValueAsBytes(object), "UTF-8");
        } catch (JsonProcessingException | UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }

}
