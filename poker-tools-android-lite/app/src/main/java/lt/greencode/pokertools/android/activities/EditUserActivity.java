package lt.greencode.pokertools.android.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import lt.greencode.pokertools.R;
import lt.greencode.pokertools.android.model.common.TaskResult;
import lt.greencode.pokertools.android.model.entities.User;
import lt.greencode.pokertools.android.tasks.HttpCallTask;
import lt.greencode.pokertools.android.utilities.MarshallingUtils;

public class EditUserActivity extends BaseActivity {

    private EditText editTextUsername;
    private EditText editTextPassword;
    private EditText editTextRepeatPassword;
    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextEmail;
    private Button buttonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_edit);

        editTextUsername = (EditText) findViewById(R.id.userEditEditTextUsername);
        editTextPassword = (EditText) findViewById(R.id.userEditEditTextPassword);
        editTextRepeatPassword = (EditText) findViewById(R.id.userEditEditTextRepeatPassword);
        editTextFirstName = (EditText) findViewById(R.id.userEditEditTextFirstName);
        editTextLastName = (EditText) findViewById(R.id.userEditEditTextLastName);
        editTextEmail = (EditText) findViewById(R.id.userEditEditTextEmail);
        buttonSave = (Button) findViewById(R.id.userEditButtonSave);

        buttonSave.setOnClickListener(new ButtonSaveListener());
    }

    class ButtonSaveListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (!editTextPassword.getText().toString().equals(editTextRepeatPassword.getText().toString())) {
                Toast.makeText(EditUserActivity.this, R.string.user_passwordsDoNotMatch, Toast.LENGTH_LONG).show();
            } else {
                User user = new User();
                user.setUsername(editTextUsername.getText().toString());
                user.setPassword(editTextPassword.getText().toString());
                user.setFirstName(editTextFirstName.getText().toString());
                user.setLastName(editTextLastName.getText().toString());
                user.setEmail(editTextEmail.getText().toString());

                String json = MarshallingUtils.convertObjectToJsonBytes(user);
                new SaveUserTask(json).execute();
            }
        }

    }

    class SaveUserTask extends HttpCallTask<User> {

        public SaveUserTask(String data) {
            super(EditUserActivity.this, "/user", PUT, JSON, JSON, data, User.class, false, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<User> result) {
            if (result.getError() == null) {
                Toast.makeText(EditUserActivity.this, R.string.user_userSuccessfullySaved, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
