package lt.greencode.pokertools.android.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Blinds implements Serializable {

    private Long id;
    private String caption;
    private List<Bet> bets = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    @Override
    public String toString() {
        return caption;
    }

}
