package lt.greencode.pokertools.android.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import lt.greencode.pokertools.R;
import lt.greencode.pokertools.android.gui.GUIElementCreators;
import lt.greencode.pokertools.android.model.common.TaskResult;
import lt.greencode.pokertools.android.model.entities.Blinds;
import lt.greencode.pokertools.android.tasks.HttpCallTask;

public class ViewBlindsActivity extends BaseActivity {

    public static final String EXTRA_BLINDS_ID = "lt.greencode.pokertools.android.activities.ViewBlindsActivity.BLINDS_ID";

    private TextView textViewCaption;
    private TableLayout tableLayoutBlinds;
    private Button buttonEdit;
    private Button buttonDelete;
    private Long blindsId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blinds_view);

        textViewCaption = (TextView) findViewById(R.id.blindsViewTextViewCaption);
        tableLayoutBlinds = (TableLayout) findViewById(R.id.blindsViewTableLayoutBlinds);
        buttonEdit = (Button) findViewById(R.id.blindsViewButtonEdit);
        buttonDelete = (Button) findViewById(R.id.blindsViewButtonDelete);

        buttonEdit.setOnClickListener(new ButtonEditListener());
        buttonDelete.setOnClickListener(new ButtonDeleteListener());

        blindsId = getIntent().getExtras().getLong(ViewBlindsActivity.EXTRA_BLINDS_ID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetBlindsTask().execute();
    }

    class ButtonEditListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ViewBlindsActivity.this, EditBlindsActivity.class);
            intent.putExtra(EditBlindsActivity.EXTRA_BLINDS_ID, blindsId);
            startActivity(intent);
        }

    }

    class ButtonDeleteListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            GUIElementCreators.showAlertDialog(ViewBlindsActivity.this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener());
        }

        class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DeleteBlindsTask().execute();
            }

        }

    }

    class GetBlindsTask extends HttpCallTask<Blinds> {

        public GetBlindsTask() {
            super(ViewBlindsActivity.this, "/blinds/" + blindsId, GET, FORM_URLENCODED, JSON, null, Blinds.class, false, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<Blinds> result) {
            Blinds blinds = result.getResult();

            textViewCaption.setText(blinds.getCaption());
            tableLayoutBlinds.removeViews(1, tableLayoutBlinds.getChildCount() - 1);
            for (int i = 0; i < blinds.getBets().size(); i++) {
                LayoutInflater inflater = (LayoutInflater) ViewBlindsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                TableRow row = (TableRow) inflater.inflate(R.layout.blinds_view_row, null);

                ((TextView) row.findViewById(R.id.blindsViewRowTextViewLevel)).setText(String.valueOf(blinds.getBets().get(i).getLevel()));
                ((TextView) row.findViewById(R.id.blindsViewRowTextViewBigBlind)).setText(blinds.getBets().get(i).getBigBlind().stripTrailingZeros().toPlainString());
                ((TextView) row.findViewById(R.id.blindsViewRowTextViewSmallBlind)).setText(blinds.getBets().get(i).getSmallBlind().stripTrailingZeros().toPlainString());
                ((TextView) row.findViewById(R.id.blindsViewRowTextViewAnte)).setText(blinds.getBets().get(i).getAnte() != null ? blinds.getBets().get(i).getAnte().stripTrailingZeros().toPlainString() : "");

                tableLayoutBlinds.addView(row);
            }
        }

    }

    class DeleteBlindsTask extends HttpCallTask<Boolean> {

        public DeleteBlindsTask() {
            super(ViewBlindsActivity.this, "/blinds/" + blindsId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(ViewBlindsActivity.this, R.string.blinds_blindsSuccessfullyDeleted, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
