package lt.greencode.pokertools.android.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class StringUtils {

    public static String getStringFromInputSream(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder result = new StringBuilder();
        String line = bufferedReader.readLine();
        while (line != null) {
            result.append(line + '\n');
            line = bufferedReader.readLine();
        }

        return result.toString();
    }

}
