package lt.greencode.pokertools.android.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.util.ArrayList;

import lt.greencode.pokertools.R;
import lt.greencode.pokertools.android.model.common.ServerError;
import lt.greencode.pokertools.android.model.common.TaskResult;
import lt.greencode.pokertools.android.model.entities.User;
import lt.greencode.pokertools.android.tasks.HttpCallTask;
import lt.greencode.pokertools.android.utilities.HttpUtils;

public class LoginActivity extends BaseActivity {

    public static final String EXTRA_JSESSION_ID = "JSESSION_ID";

    private EditText editTextUsername;
    private EditText editTextPassword;
    private Button buttonLoginAsUser;
    private Button buttonCreateAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        editTextUsername = (EditText) findViewById(R.id.loginEditTextUsername);
        editTextPassword = (EditText) findViewById(R.id.loginEditTextPassword);
        buttonLoginAsUser = (Button) findViewById(R.id.loginButtonLoginAsUser);
        buttonCreateAccount = (Button) findViewById(R.id.loginButtonCreateAccount);

        buttonLoginAsUser.setOnClickListener(new ButtonLoginAsUserListener());
        buttonCreateAccount.setOnClickListener(new ButtonCreateAccountListener());
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("lt.greencode.pokertools", Context.MODE_PRIVATE);
        sharedPreferences.edit().remove(EXTRA_JSESSION_ID).apply();
    }

    class ButtonLoginAsUserListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            String data = String.format("{\"username\": \"%s\",\"password\": \"%s\"}", editTextUsername.getText().toString(), editTextPassword.getText().toString());
            new LoginTask(data).execute();
        }

    }

    class ButtonCreateAccountListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            startActivity(new Intent(LoginActivity.this, EditUserActivity.class));
        }

    }

    class LoginTask extends HttpCallTask<User> {

        public LoginTask(String data) {
            super(LoginActivity.this, "/auth/login", POST, JSON, JSON, data, String.class, false, true, true);
        }

        @Override
        protected TaskResult<User> doInBackground(String... params) {
            try {
                return HttpUtils.doHttpCall(SERVER_URL + httpAddress, httpMethod, contentType, acceptType, data, resultClass, isList, null, true);
            } catch (SecurityException ex) {
                return new TaskResult<>(new ServerError(LoginActivity.this.getString(R.string.login_invalidUsernameOrPassword)));
            } catch (IOException ex) {
                return new TaskResult<>(new ServerError(LoginActivity.this.getString(R.string.common_canNotConnectToTheServer)));
            }
        }

        @Override
        protected void postExecuteActions(TaskResult<User> result) {
            SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.this.getPackageName(), Context.MODE_PRIVATE);
            sharedPreferences.edit().putString(EXTRA_JSESSION_ID, result.getSessionId()).apply();
            startActivity(new Intent(LoginActivity.this, MenuActivity.class));
        }

    }

}
