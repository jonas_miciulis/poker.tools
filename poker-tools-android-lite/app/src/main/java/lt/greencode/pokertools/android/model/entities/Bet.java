package lt.greencode.pokertools.android.model.entities;

import java.io.Serializable;
import java.math.BigDecimal;

public class Bet implements Serializable {

    private Long id;
    private Integer level;
    private BigDecimal bigBlind;
    private BigDecimal smallBlind;
    private BigDecimal ante;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public BigDecimal getBigBlind() {
        return bigBlind;
    }

    public void setBigBlind(BigDecimal bigBlind) {
        this.bigBlind = bigBlind;
    }

    public BigDecimal getSmallBlind() {
        return smallBlind;
    }

    public void setSmallBlind(BigDecimal smallBlind) {
        this.smallBlind = smallBlind;
    }

    public BigDecimal getAnte() {
        return ante;
    }

    public void setAnte(BigDecimal ante) {
        this.ante = ante;
    }

}
