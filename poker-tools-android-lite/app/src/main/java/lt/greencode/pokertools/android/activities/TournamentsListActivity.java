package lt.greencode.pokertools.android.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import lt.greencode.pokertools.R;
import lt.greencode.pokertools.android.gui.GUIElementCreators;
import lt.greencode.pokertools.android.model.common.TaskResult;
import lt.greencode.pokertools.android.model.entities.Tournament;
import lt.greencode.pokertools.android.tasks.HttpCallTask;

public class TournamentsListActivity extends BaseActivity {

    private ListView listViewTournaments;
    private Button buttonNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tournaments_list);

        listViewTournaments = (ListView) findViewById(R.id.tournamentsListListViewTournaments);
        buttonNew = (Button) findViewById(R.id.tournamentsListButtonNew);

        listViewTournaments.setOnItemClickListener(new ListViewTournamentsListener());
        buttonNew.setOnClickListener(new ButtonNewListener());

        registerForContextMenu(listViewTournaments);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Tournament tournament = (Tournament) ((ListView) view).getAdapter().getItem(info.position);
        menu.setHeaderTitle(tournament.getCaption());

        menu.add(Menu.NONE, tournament.getId().intValue(), 1, getString(R.string.common_view));
        menu.add(Menu.NONE, tournament.getId().intValue(), 2, getString(R.string.common_edit));
        menu.add(Menu.NONE, tournament.getId().intValue(), 3, getString(R.string.common_delete));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(getString(R.string.common_view))) {
            Intent intent = new Intent(this, ViewTournamentActivity.class);
            intent.putExtra(ViewTournamentActivity.EXTRA_TOURNAMENT_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_edit))) {
            Intent intent = new Intent(this, EditTournamentActivity.class);
            intent.putExtra(EditTournamentActivity.EXTRA_TOURNAMENT_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_delete))) {
            GUIElementCreators.showAlertDialog(this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener((long) item.getItemId()));
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetTournamentsTask().execute();
    }

    class ListViewTournamentsListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Tournament tournament = (Tournament) parent.getItemAtPosition(position);
            Intent intent = new Intent(TournamentsListActivity.this, ViewTournamentActivity.class);
            intent.putExtra(ViewTournamentActivity.EXTRA_TOURNAMENT_ID, tournament.getId());
            startActivity(intent);
        }

    }

    class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

        private Long tournamentId;

        public ButtonConfirmDeleteListener(Long tournamentId) {
            this.tournamentId = tournamentId;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            new DeleteTournamentTask(tournamentId).execute();
            new GetTournamentsTask().execute();
        }

    }

    class ButtonNewListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            startActivity(new Intent(TournamentsListActivity.this, EditTournamentActivity.class));
        }

    }

    class GetTournamentsTask extends HttpCallTask<List<Tournament>> {

        public GetTournamentsTask() {
            super(TournamentsListActivity.this, "/tournament", GET, FORM_URLENCODED, JSON, null, Tournament.class, true, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<List<Tournament>> result) {
            listViewTournaments.setAdapter(new ArrayAdapter<>(TournamentsListActivity.this, android.R.layout.simple_list_item_1, result.getResult()));
        }

    }

    class DeleteTournamentTask extends HttpCallTask<Boolean> {

        public DeleteTournamentTask(long tournamentId) {
            super(TournamentsListActivity.this, "/tournament/" + tournamentId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(TournamentsListActivity.this, R.string.tournament_tournamentSuccessfullyDeleted, Toast.LENGTH_LONG).show();
            }
        }

    }

}
