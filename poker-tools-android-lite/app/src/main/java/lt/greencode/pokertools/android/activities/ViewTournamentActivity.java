package lt.greencode.pokertools.android.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import lt.greencode.pokertools.R;
import lt.greencode.pokertools.android.gui.GUIElementCreators;
import lt.greencode.pokertools.android.model.common.TaskResult;
import lt.greencode.pokertools.android.model.entities.Tournament;
import lt.greencode.pokertools.android.tasks.HttpCallTask;

public class ViewTournamentActivity extends BaseActivity {

    public static final String EXTRA_TOURNAMENT_ID = "TOURNAMENT_ID";

    private TextView textViewId;
    private TextView textViewCaption;
    private TextView textViewLevelTime;
    private TextView textViewChipsAtBeginning;
    private TextView textViewBlinds;
    private Button buttonEdit;
    private Button buttonDelete;
    private Long tournamentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tournament_view);

        textViewId = (TextView) findViewById(R.id.tournamentViewTextViewId);
        textViewCaption = (TextView) findViewById(R.id.tournamentViewTextViewCaption);
        textViewLevelTime = (TextView) findViewById(R.id.tournamentViewTextViewLevelTime);
        textViewChipsAtBeginning = (TextView) findViewById(R.id.tournamentViewTextViewChipsAtBeginning);
        textViewBlinds = (TextView) findViewById(R.id.tournamentViewTextViewBlinds);
        buttonEdit = (Button) findViewById(R.id.tournamentViewButtonEdit);
        buttonDelete = (Button) findViewById(R.id.tournamentViewButtonDelete);

        buttonEdit.setOnClickListener(new ButtonEditListener());
        buttonDelete.setOnClickListener(new ButtonDeleteListener());

        tournamentId = getIntent().getExtras().getLong(ViewTournamentActivity.EXTRA_TOURNAMENT_ID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetTournamentTask().execute();
    }

    class ButtonEditListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(ViewTournamentActivity.this, EditTournamentActivity.class);
            intent.putExtra(EditTournamentActivity.EXTRA_TOURNAMENT_ID, tournamentId);
            startActivity(intent);
        }

    }

    class ButtonDeleteListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            GUIElementCreators.showAlertDialog(ViewTournamentActivity.this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener());
        }

        class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DeleteTournamentTask().execute();
            }

        }

    }

    class GetTournamentTask extends HttpCallTask<Tournament> {

        public GetTournamentTask() {
            super(ViewTournamentActivity.this, "/tournament/" + tournamentId, GET, FORM_URLENCODED, JSON, null, Tournament.class, false, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<Tournament> result) {
            Tournament tournament = result.getResult();

            textViewId.setText(String.valueOf(tournament.getId()));
            textViewCaption.setText(tournament.getCaption());
            textViewLevelTime.setText(String.valueOf(tournament.getLevelTime()));
            textViewChipsAtBeginning.setText(tournament.getChipsAtBeginning() != null ? String.valueOf(tournament.getChipsAtBeginning()) : "");
            textViewBlinds.setText(tournament.getBlinds().getCaption());
        }

    }

    class DeleteTournamentTask extends HttpCallTask<Boolean> {

        public DeleteTournamentTask() {
            super(ViewTournamentActivity.this, "/tournament/" + tournamentId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(ViewTournamentActivity.this, R.string.tournament_tournamentSuccessfullyDeleted, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
