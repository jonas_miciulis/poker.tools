package lt.greencode.pokertools.android.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import lt.greencode.pokertools.R;
import lt.greencode.pokertools.android.model.common.TaskResult;
import lt.greencode.pokertools.android.model.entities.Blinds;
import lt.greencode.pokertools.android.model.entities.Tournament;
import lt.greencode.pokertools.android.tasks.HttpCallTask;
import lt.greencode.pokertools.android.utilities.MarshallingUtils;

public class EditTournamentActivity extends BaseActivity {

    public static final String EXTRA_TOURNAMENT_ID = "TOURNAMENT_ID";
    public static final String EXTRA_BLINDS = "BLINDS";

    private static final long EMPTY_ITEM_ID = 0L;

    private EditText editTextCaption;
    private EditText editTextLevelTime;
    private EditText editTextChipsAtBeginning;
    private Spinner spinnerBlinds;
    private Button buttonSave;
    private List<Blinds> blinds;
    private Tournament tournament;
    private Long tournamentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tournament_edit);

        editTextCaption = (EditText) findViewById(R.id.tournamentEditEditTextCaption);
        editTextLevelTime = (EditText) findViewById(R.id.tournamentEditEditTextLevelTime);
        editTextChipsAtBeginning = (EditText) findViewById(R.id.tournamentEditEditTextChipsAtBeginning);
        spinnerBlinds = (Spinner) findViewById(R.id.tournamentEditSpinnerBlinds);
        buttonSave = (Button) findViewById(R.id.tournamentEdittButtonSave);

        buttonSave.setOnClickListener(new ButtonSaveListener());

        new GetBlindsTask(null).execute();

        if (getIntent().hasExtra(EditTournamentActivity.EXTRA_TOURNAMENT_ID)) {
            tournamentId = getIntent().getExtras().getLong(EditTournamentActivity.EXTRA_TOURNAMENT_ID);
            new GetTournamentTask().execute();
        }
    }

    class ButtonSaveListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Tournament tournament = new Tournament();

            String caption = editTextCaption.getText().toString();
            String levelTime = editTextLevelTime.getText().toString();
            String chipsAtBeginning = editTextChipsAtBeginning.getText().toString();

            tournament.setId(tournamentId);
            tournament.setCaption(caption);
            tournament.setLevelTime(!levelTime.isEmpty() ? Integer.valueOf(levelTime) : null);
            tournament.setChipsAtBeginning(!chipsAtBeginning.isEmpty() ? Integer.valueOf(chipsAtBeginning) : null);

            Blinds selectedBlinds = (Blinds) spinnerBlinds.getSelectedItem();
            tournament.setBlinds(selectedBlinds.getId() != EMPTY_ITEM_ID ? selectedBlinds : null);

            String json = MarshallingUtils.convertObjectToJsonBytes(tournament);
            new SaveTournamentTask(json).execute();
        }

    }

    class GetTournamentTask extends HttpCallTask<Tournament> {

        public GetTournamentTask() {
            super(EditTournamentActivity.this, "/tournament/" + tournamentId, GET, FORM_URLENCODED, JSON, null, Tournament.class, false, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<Tournament> result) {
            tournament = result.getResult();

            editTextCaption.setText(tournament.getCaption());
            editTextLevelTime.setText(String.valueOf(tournament.getLevelTime()));
            editTextChipsAtBeginning.setText(tournament.getChipsAtBeginning() != null ? String.valueOf(tournament.getChipsAtBeginning()) : "");

            if (blinds != null) {
                spinnerBlinds.setSelection(blinds.indexOf(tournament.getBlinds()));
            }
        }

    }

    class GetBlindsTask extends HttpCallTask<List<Blinds>> {

        private Blinds blindsToSelect;

        public GetBlindsTask(Blinds blinds) {
            super(EditTournamentActivity.this, "/blinds", GET, FORM_URLENCODED, JSON, null, Blinds.class, true, true, false);
            this.blindsToSelect = blinds;
        }

        @Override
        protected void postExecuteActions(TaskResult<List<Blinds>> result) {
            blinds = result.getResult();

            ArrayAdapter<Blinds> spinnerBlindsDataAdapter = new ArrayAdapter<>(EditTournamentActivity.this, android.R.layout.simple_spinner_dropdown_item, blinds);
            spinnerBlinds.setAdapter(spinnerBlindsDataAdapter);

            if (blindsToSelect != null) {
                spinnerBlinds.setSelection(blinds.indexOf(blindsToSelect));
            } else if (tournament != null) {
                spinnerBlinds.setSelection(blinds.indexOf(tournament.getBlinds()));
            }
        }

    }

    class SaveTournamentTask extends HttpCallTask<Tournament> {


        public SaveTournamentTask(String data) {
            super(EditTournamentActivity.this, "/tournament", PUT, JSON, JSON, data, Tournament.class, false, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<Tournament> result) {
            if (result.getError() == null) {
                Toast.makeText(EditTournamentActivity.this, R.string.tournament_tournamentSuccessfullySaved, Toast.LENGTH_LONG).show();
                finish();
            }
        }

    }

}
