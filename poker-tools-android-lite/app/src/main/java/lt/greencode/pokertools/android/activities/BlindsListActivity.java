package lt.greencode.pokertools.android.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import lt.greencode.pokertools.R;
import lt.greencode.pokertools.android.gui.GUIElementCreators;
import lt.greencode.pokertools.android.model.common.TaskResult;
import lt.greencode.pokertools.android.model.entities.Blinds;
import lt.greencode.pokertools.android.tasks.HttpCallTask;

public class BlindsListActivity extends BaseActivity {

    private ListView listViewBlinds;
    private Button buttonNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blinds_list);

        listViewBlinds = (ListView) findViewById(R.id.blindsListListViewBlinds);
        buttonNew = (Button) findViewById(R.id.blindsListButtonNew);

        listViewBlinds.setOnItemClickListener(new ListViewBlindsListener());
        buttonNew.setOnClickListener(new ButtonNewListener());

        registerForContextMenu(listViewBlinds);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Blinds blinds = (Blinds) ((ListView) view).getAdapter().getItem(info.position);
        menu.setHeaderTitle(blinds.getCaption());

        menu.add(Menu.NONE, blinds.getId().intValue(), 1, getString(R.string.common_view));
        menu.add(Menu.NONE, blinds.getId().intValue(), 2, getString(R.string.common_edit));
        menu.add(Menu.NONE, blinds.getId().intValue(), 3, getString(R.string.common_delete));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(getString(R.string.common_view))) {
            Intent intent = new Intent(this, ViewBlindsActivity.class);
            intent.putExtra(ViewBlindsActivity.EXTRA_BLINDS_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_edit))) {
            Intent intent = new Intent(this, EditBlindsActivity.class);
            intent.putExtra(EditBlindsActivity.EXTRA_BLINDS_ID, (long) item.getItemId());
            startActivity(intent);
        } else if (item.getTitle().equals(getString(R.string.common_delete))) {
            GUIElementCreators.showAlertDialog(this, getString(R.string.common_deleteConfirmation), new ButtonConfirmDeleteListener((long) item.getItemId()));
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetBlindsTask().execute();
    }

    class ListViewBlindsListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Blinds blinds = (Blinds) parent.getItemAtPosition(position);
            Intent intent = new Intent(BlindsListActivity.this, ViewBlindsActivity.class);
            intent.putExtra(ViewBlindsActivity.EXTRA_BLINDS_ID, blinds.getId());
            startActivity(intent);
        }

    }

    class ButtonConfirmDeleteListener implements DialogInterface.OnClickListener {

        private Long blindsId;

        public ButtonConfirmDeleteListener(Long blindsId) {
            this.blindsId = blindsId;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            new DeleteBlindsTask(blindsId).execute();
            new GetBlindsTask().execute();
        }

    }

    class ButtonNewListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            startActivity(new Intent(BlindsListActivity.this, EditBlindsActivity.class));
        }

    }

    class GetBlindsTask extends HttpCallTask<List<Blinds>> {

        public GetBlindsTask() {
            super(BlindsListActivity.this, "/blinds", GET, FORM_URLENCODED, JSON, null, Blinds.class, true, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<List<Blinds>> result) {
            listViewBlinds.setAdapter(new ArrayAdapter<>(BlindsListActivity.this, android.R.layout.simple_list_item_1, result.getResult()));
        }

    }

    class DeleteBlindsTask extends HttpCallTask<Boolean> {

        public DeleteBlindsTask(long blindsId) {
            super(BlindsListActivity.this, "/blinds/" + blindsId, DELETE, FORM_URLENCODED, JSON, null, Boolean.class, false, true, false);
        }

        @Override
        protected void postExecuteActions(TaskResult<Boolean> result) {
            if (result.getError() == null) {
                Toast.makeText(BlindsListActivity.this, R.string.blinds_blindsSuccessfullyDeleted, Toast.LENGTH_LONG).show();
            }
        }

    }

}
