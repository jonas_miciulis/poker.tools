package lt.greencode.pokertools.android.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import lt.greencode.pokertools.R;
import lt.greencode.pokertools.android.activities.BaseActivity;
import lt.greencode.pokertools.android.activities.LoginActivity;
import lt.greencode.pokertools.android.model.common.ServerError;
import lt.greencode.pokertools.android.model.common.TaskResult;
import lt.greencode.pokertools.android.utilities.HttpUtils;

public abstract class HttpCallTask<T> extends AsyncTask<String, Void, TaskResult<T>> {

    protected Activity activity;
    protected ProgressDialog progressDialog;
    protected String httpAddress;
    protected String httpMethod;
    protected String acceptType;
    protected String contentType;
    protected String data;
    protected Class resultClass;
    protected boolean isList;
    protected boolean showSpinner;
    protected boolean isLogin;

    public HttpCallTask(Activity activity, String httpAddress, String httpMethod, String contentType, String acceptType, String data, Class resultClass, boolean isList, boolean showSpinner, boolean isLogin) {
        this.activity = activity;
        this.httpAddress = httpAddress;
        this.httpMethod = httpMethod;
        this.acceptType = acceptType;
        this.contentType = contentType;
        this.data = data;
        this.resultClass = resultClass;
        this.isList = isList;
        this.showSpinner = showSpinner;
        this.isLogin = isLogin;
    }

    @Override
    protected void onPreExecute() {
        if (showSpinner) {
            progressDialog = ProgressDialog.show(activity, "", activity.getString(R.string.common_pleaseWait), true);
        }
    }

    @Override
    protected TaskResult<T> doInBackground(String... params) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(activity.getPackageName(), Context.MODE_PRIVATE);
        String sessionId = sharedPreferences.getString(LoginActivity.EXTRA_JSESSION_ID, null);

        try {
            return HttpUtils.doHttpCall(BaseActivity.SERVER_URL + httpAddress, httpMethod, contentType, acceptType, data, resultClass, isList, sessionId, isLogin);
        } catch (SecurityException ex) {
            return new TaskResult<T>(new ServerError(activity.getString(R.string.common_sessionFinished)));
        } catch (IOException ex) {
            return new TaskResult<T>(new ServerError(activity.getString(R.string.common_canNotConnectToTheServer)));
        }
    }

    @Override
    protected void onPostExecute(TaskResult<T> result) {
        if (showSpinner && !activity.isDestroyed()) {
            progressDialog.dismiss();
        }

        if (result.getError() == null) {
            postExecuteActions(result);
        } else {
            Toast.makeText(activity, result.getError().toString(), Toast.LENGTH_LONG).show();
        }
    }

    protected abstract void postExecuteActions(TaskResult<T> result);

}

